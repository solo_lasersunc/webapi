﻿using Amazon.SQS;
using Amazon.SQS.Model;
using Newtonsoft.Json;
using System;
using System.Configuration;
using System.Web.Http;
using WebApi.Models;

namespace WebApi.Controllers
{
    public class SmsController : ApiController
    {
        [Authorize]
        public IHttpActionResult Post(SmsModel smsModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                ////should create a service and move below code to service
                var request = new SendMessageRequest(
                    queueUrl: ConfigurationManager.AppSettings["QueueUrl"],
                    messageBody: JsonConvert.SerializeObject(smsModel));
                var client = new AmazonSQSClient();
                var response = client.SendMessage(request);

                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ToString());
            }

        }
    }
}
﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace WebApi.Models
{
    public class EmailMessage
    {
        [Required]
        public string subject { get; set; }
        [Required]
        public string body { get; set; }       
    }
}
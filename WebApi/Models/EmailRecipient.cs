﻿using System.ComponentModel.DataAnnotations;

namespace WebApi.Models
{
    public class EmailRecipient
    {
        [Required]
        [EmailAddress]
        public string from { get; set; }

        [Required]
        [EmailAddress]
        public string to { get; set; }
    }
}
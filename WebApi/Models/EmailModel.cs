﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApi.Models
{
    public class EmailModel
    {
        public EmailModel ()
        {
            recipients = new EmailRecipient();
            message = new EmailMessage();
        }

        [Required]
        public EmailRecipient recipients { get; set; }
        [Required]
        public EmailMessage message { get; set; }
        [Required]
        public string deliveryMethod { get; set; }
    }
}
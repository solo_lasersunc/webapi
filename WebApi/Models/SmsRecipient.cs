﻿

using System.ComponentModel.DataAnnotations;

namespace WebApi.Models
{
    public class SmsRecipient
    {
        [Required]
        [PhoneAttribute]
        public string to { get; set; }
    }
}
﻿

using System.ComponentModel.DataAnnotations;

namespace WebApi.Models
{
    public class SmsMessage
    {
        [Required]
        [StringLength(160, ErrorMessage = "The message should not more than 160 characters")]
        public string body { get; set; }
    }
}
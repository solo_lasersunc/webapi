﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApi.Models
{
    public class SmsModel
    {
        [Required]
        public SmsRecipient recipients { get; set; }
        [Required]
        public SmsMessage message { get; set; }
        [Required]
        public string deliveryMethod { get; set; }
    }
}